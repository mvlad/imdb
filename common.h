#ifndef __COMMON_H
#define __COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdint.h>
#include <stddef.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/select.h>
#include <syslog.h>
#include <ctype.h>
#include <sys/syscall.h>

#include <sys/un.h>

#include "list.h"

extern unsigned int debug;
extern unsigned int run_as_daemon;

#define SYSLOG(fmt, args...) \
        syslog(LOG_USER | LOG_NDELAY | LOG_INFO, fmt, ##args);

#define LOG(fmt, args...)    do {               \
        if (debug && !run_as_daemon) {          \
                fprintf(stderr, fmt, ##args);   \
        } else {                                \
                SYSLOG(fmt, ##args)             \
        }                                       \
} while (0)

#ifdef  EDEBUG                             
#define DEBUG(fmt, args...)     do {            \
        if (debug && !run_as_daemon) {          \
                fprintf(stderr, fmt, ##args);   \
        } else {                                \
                SYSLOG(fmt, ##args)             \
        }                                       \
} while (0)
#else
#define DEBUG(fmt, args...)     do {            \
} while (0)
#endif

#define FATAL(fmt, args...)    do {     \
        fprintf(stderr, fmt, ##args);   \
        exit(1);                        \
} while (0)

#define VERSION         0.9995
#define BUFLEN          1024 * 4
#define ONE_MILLION     1000 * 1000

#define THREAD_COUNT            1
#define STR(x)  #x
#define XSTR(x) STR(x)
#define STATUS_INTERVAL         (30 * 60 * 1000)
#define THREAD_STATUS_INTERVAL  (60 * 1000)

typedef struct rthread {
        pid_t                   thread;
        struct list_head        list;
} rthread_t;

typedef struct thread_intern { 
        int sfd;
        int psock;
        int nr;
} thread_intern_t;

static inline void get_current_ts(struct timeval *tv)
{
        struct timespec ts;

        if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1) {
                FATAL("Could not get time of day: %s\n", strerror(errno));
        }

        tv->tv_sec      = ts.tv_sec;
        tv->tv_usec     = ts.tv_nsec / 1000;
}

/* ret difference in ms */
#define tv_diff(t1, t2) ((((t1)->tv_sec - (t2)->tv_sec) * 1000) + (((t1)->tv_usec - (t2)->tv_usec) / 1000))

static inline int tv_cmp(struct timeval *t1, struct timeval *t2)
{
        if (t1->tv_sec < t2->tv_sec) {
                return -1;
        }
        if (t1->tv_sec > t2->tv_sec) {
                return 1;
        }

        if (t1->tv_usec < t2->tv_usec) {
                return -1;
        }
        if (t1->tv_usec > t2->tv_usec) {
                return 1;
        }

        return 0;
}

static void inline add_tv_to_tv(struct timeval *tv1, struct timeval *tv2)
{
        tv1->tv_sec  += tv2->tv_sec;
        tv1->tv_usec += tv2->tv_usec;

        if (tv1->tv_usec >= ONE_MILLION) {
                tv1->tv_sec++;
                tv1->tv_usec = tv1->tv_usec % ONE_MILLION;
        }
}


#endif
