CC = gcc
#CFLAGS = -Wall -O2 -m32 -march=i686
CFLAGS = -Wall -O0 -m32 -march=i686
WITH_DEBUG = -g
LIBS = -pthread -lrt
#PERL_LDFLAGS = $(shell perl -MExtUtils::Embed -e ldopts)
#PERL_LDFLAGS = -Wl,-E -L/usr/local/lib -LL/usr/lib/perl/5.14/CORE -lperl -lnsl -ldl -lm -lcrypt -lutil -lc
PERL_LDFLAGS = -Wl,-E  -L/usr/local/lib  -L/home/mvlad/perl5/perlbrew/perls/perl-5.14.3/lib/5.14.3/x86_64-linux/CORE -lperl -lnsl -ldl -lm -lcrypt -lutil -lc
#PERL_CFLAGS = $(shell perl -MExtUtils::Embed -e ccopts)
#PERL_CFLAGS = -fno-strict-aliasing -pipe -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -I/usr/lib/perl/5.14/CORE
PERL_CFLAGS = -fno-strict-aliasing -pipe -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64  -I/home/mvlad/perl5/perlbrew/perls/perl-5.14.3/lib/5.14.3/x86_64-linux/CORE

INCLUDES = -I. $(PERL_CFLAGS)

override CFLAGS += -D_GNU_SOURCE -D_REENTRANT

#BUILDVERSION    := $(shell git rev-list --max-count=1 --pretty="format:%h" HEAD | tail -1)
override CFLAGS += -DBUILDVERSION="$(BUILDVERSION)"

IMDB_SRC 	= xutil.c eperl.c timer.c htable.c parse_cmd.c conn.c imdb.c
IMDB_O		= $(patsubst %.c,%.o,$(IMDB_SRC))

TARGETS = imdb cimdb manpage


all:	$(TARGETS)

.c.o:
	$(CC) $(CFLAGS) $(LIBS) $(INCLUDES) $(WITH_DEBUG) -c -o $@ $<

imdb:	$(IMDB_O)
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS) $(PERL_LDFLAGS) $(WITH_DEBUG)

cimdb:	cimdb.o xutil.o
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS) $(PERL_LDFLAGS) $(WITH_DEBUG)

clean:
	@rm -rf core imdb $(IMDB_O) cimdb.o cimdb xutil.o man/imdb.8

manpage: 
	@pod2man --release='InMemoryDatabase' -n 'imdb' --center='IMDB ($(BUILDVERSION))' --section=8 man/imdb.pod > man/imdb.8

distclean:	clean
	-rm -rf TAGS
deb:
	@dpkg-buildpackage -us -uc -b -rfakeroot
source:
	@dpkg-buildpackage -us -uc -S -I -I.git -I -Iextra -I -I.gitignore -I -I.gdbinit -I -Itests
tags:
	ctags *.[ch]
