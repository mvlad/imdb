#ifndef __HTABLE_H
#define __HTABLE_H

#define HASH_GROW_THRESHOLD     75
#define N_BUCKETS               1024

typedef unsigned int (*k_hash_func)(void *);
typedef int (*k_compare_func)(void *, void *);
typedef void (*k_val_destroy_func)(void *, void *);

typedef struct {
        struct list_head list;

        void *key;
        void *val;
} hash_entry_t;

typedef struct {
        unsigned int            n_buckets;
        unsigned int            n_entries;
        struct list_head        *buckets;

        k_hash_func             hash_func;
        k_compare_func          compare_func;
        k_val_destroy_func      destroy_func;
} hash_internal_t;

extern hash_internal_t 
**hash_create(unsigned int buckets, 
              unsigned int thread_cnt,
              k_hash_func hash_func, 
              k_compare_func compare_func,
              k_val_destroy_func destroy_func);

extern void
*hash_lookup(hash_internal_t **hash, void *key);

void hash_insert(hash_internal_t **hash, void *key, void *val);
void hash_remove(hash_internal_t **hash, void *key);
void hash_destroy(hash_internal_t **hash);

#endif
