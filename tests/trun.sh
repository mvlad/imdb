#/bin/bash

trap 'killall cimdb && exit 0' SIGINT

if [ -z "$1" ]; then
        exit 123
fi

PORT="3769"
HOST="$1"
RUNNER="../cimdb"

for j in `seq 1 15`; do
        echo "# testing $j"
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
        (
         $RUNNER -c store -s $HOST -p $PORT -i "$(head -c 100 /dev/urandom | tr -dc 'a-zA-Z0-9##@#%%')" \
                -f "$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@@#&&*')"
        ) &
done

while [ "`pidof cimdb | wc -c`" = "1" ]; do
        killall cimdb
done
