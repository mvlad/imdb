#!/bin/bash

if [ $# -eq 0 ]; then
        echo "invalid number of arguments"
        exit 123
fi

RUNNER="`pwd`/../cimdb"
IP="$1"
PORT="3769"

if [ -z "$IP" ]; then
       exit 124 
fi

function store()
{
        $RUNNER -c "store" -s $IP -i $1 -f $2 -p $PORT
}

function retrieve()
{
        $RUNNER -c "retrieve" -s $IP -i $1 -f $2 -p $PORT
}

for j in `seq 1 5`; do
        FLAGID="FLAGID$RANDOM"
        FLAG=$(head -c 200 /dev/urandom | tr -dc 'a-zA-Z0-9#@!@#&&*' | sha1sum)
        store $FLAGID $FLAG
        sleep 1
        retrieve $FLAGID $FLAG
        sleep 1
done
