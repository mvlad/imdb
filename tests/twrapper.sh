#!/bin/bash

if [ $# -eq 0 ]; then
        echo "invalid number of arguments"
        exit 123
fi

RUNNER="`dirname $0`/cimdb"
COMMAND="$1"
IP="$2"
FLAGID="$3"
FLAG="$4"
PORT="3769"

if [ -z "$IP" ]; then
       exit 124 
fi

function store()
{
        $RUNNER -c "store" -s $1 -i $2 -f $3 -p $PORT
}

function retrieve()
{
        $RUNNER -c "retrieve" -s $1 -i $2 -f $3 -p $PORT
}

function extractBase64()
{
        IN="$1"
        sed -i -re 's/.*base64,//; /^<|"[[:space:]]\/>/d' $IN > $OUT
}

function decodeBase64()
{
        base64 -d $1 > $2
}

function convert2PNG()
{
        TMP="/tmp/dump.html"

        wget -q $1:8081 -O $TMP

        OUT="$2"
        extractBase64 $TMP
        decodeBase64 "$TMP" "$OUT"
}

function alive()
{
        FILE="/tmp/down.png"

	cmd="`HEAD http://$1:8081 | grep 'Server' | awk -F ':' '/nginx/{ print "y"}'`"

	# conn err if the frontend is down
	if [ $cmd != "y" ]; then
		exit 1
	fi

        convert2PNG $1 $FILE

	if [ `exiftool $FILE | grep 'Keywords' -c` -ne 1 ]; then
		exit 3
	fi
}


case "$1" in
        store)
		alive $IP
                store $IP $FLAGID $FLAG
        ;;
        retrieve)
		alive $IP
                retrieve $IP $FLAGID $FLAG
        ;;
        *)
                exit 123;
esac
