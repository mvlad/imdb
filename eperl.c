#include "eperl.h"
#include "common.h"
#include "xutil.h"

static PerlInterpreter *my_perl;
extern char *perlmmap;

void initPerl(int argc, char *argv[], char *env[])
{
        static char *embeded[] = { "", "-e", "0" };

        PERL_SYS_INIT3(&argc, &argv, &env);
        my_perl = perl_alloc();
        perl_construct(my_perl);

        perl_parse(my_perl, NULL, 3, embeded, env);

        PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
        perl_run(my_perl);
}

void runAsPerl(char *ecode, char **result)
{
        SV *val = eval_pv(ecode, TRUE);
        *result = SvPV_nolen(val);
}

void destroyPerl(void)
{
        perl_destruct(my_perl);
        perl_free(my_perl);
        PERL_SYS_TERM();
}

void bootstrapPerl(int p_sock)
{
        int afd = 0;
        socklen_t addrlen;
        struct sockaddr_un name;

        while (1) {
                int r, nread, nfds = 0;
                fd_set rfd;
                char buf[BUFLEN];
                char *result;

                FD_ZERO(&rfd);
                FD_SET(p_sock, &rfd);

                nfds = MAX(nfds, p_sock);

                r = pselect(nfds + 1, &rfd, NULL, NULL, NULL, NULL);

                if (r == -1 && errno == EINTR) {
                        continue;
                }

                if (r == -1) {
                        LOG("# bootstrapPerl() pselect() failed.\n");
                }

                if (FD_ISSET(p_sock, &rfd)) {

                        afd = accept(p_sock, (struct sockaddr *) &name, &addrlen);
                        if (afd == -1) {
                                LOG("# bootstrapPerl() accept(): '%s'\n", strerror(errno));
                        }

                        FD_SET(afd, &rfd);
                        nfds = MAX(nfds, afd);
                }

                if (FD_ISSET(afd, &rfd)) {
more:
                        memset(buf, 0, BUFLEN);
                        while ((nread = read(afd, &buf, BUFLEN)) > 0) {

                                /* 
                                 * FIXME maybe dupping 0, 1, 2 we can catch the
                                 * error from perl 
                                 */

                                runAsPerl(buf, &result);

                                /* send data back data */
                                int towrite = xstrlen(result);
                                if (towrite > 0) {
                                        if (write(afd, result, towrite) != towrite) {
                                                LOG("# failed to send data to client\n");
                                        }
                                } else {
                                        LOG("# perl didn't returned anything\n");
                                        close(afd);
                                }
                        }

                        if (nread < 0 && errno == EINTR) {
                                goto more;
                        }
                        if (nread <= 0) {
                                LOG("# client closed connection over UNIX sock\n");
                                close(afd);
                        }
                }
        }
}
