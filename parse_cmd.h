#ifndef __PARSE_CMD
#define __PARSE_CMD

extern char *get_first_val(char *buf, const char *str, char delim);
extern char **split_delim(char *str, char *delim, int *nvals);
extern void free_delim(char **xstr, int len);

#endif
