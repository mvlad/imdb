#!/usr/bin/perl
use warnings;
use strict;



__END__

This is the 0 challenge exploiting the fact the 
the service allows listing of keys from
any address connecting to the service.

This should be fixed by only allowing
``list'' (and probably ``count'') command only
from localhost.
