#include <stdio.h>
#include <stdlib.h>

#include <EXTERN.h>
#include <perl.h>

static PerlInterpreter *eperl;
int main(int argc, char *argv[], char *env[])
{
        eperl = perl_alloc();
        perl_construct(eperl);

        perl_parse(eperl, NULL, argc, argv, env);

        perl_run(eperl);
        perl_destruct(eperl);
        perl_free(eperl);

        return 0;
}
