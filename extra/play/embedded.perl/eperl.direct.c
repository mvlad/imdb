#include <stdio.h>
#include <stdlib.h>

#include <EXTERN.h>
#include <perl.h>

static PerlInterpreter *my_perl;
static char *embeded[] = { "", "-e", "0" };
static char *pcode = "$a = sub { print $_, \"\n\" for (1..100); }";
static char *p2code = "use feature 'say'; $a = 34; $b = 12; $a = $a + $b; say $a;";

int main(int argc, char *argv[], char *env[])
{
        PERL_SYS_INIT3(&argc, &argv, &env);
        my_perl = perl_alloc();
        perl_construct(my_perl);

        perl_parse(my_perl, NULL, 3, embeded, env);
        PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
        perl_run(my_perl);

        eval_pv("$a = 3; $a **= 2 * $a", TRUE);
        fprintf(stderr, "# a has %d\n", SvIV(get_sv("a", 0)));

        SV *cvrv = eval_pv(pcode, TRUE);
        char *res = SvPV_nolen(get_sv("a", 0));

        fprintf(stderr, "# len of the output %d\n", strlen(res));
        fprintf(stderr, ">>> start of output <<<\n");
        char *pptr = res;

        while (pptr && *pptr) {
                fprintf(stderr, "# looking at %c\n", *pptr);
                char *sx = pptr;        
                if (*sx == '\n') {
                        fprintf(stderr, "# found new line\n");
                }
                pptr++;
        }

        fprintf(stderr, ">>> end of output <<<\n");

        perl_destruct(my_perl);
        perl_free(my_perl);
        PERL_SYS_TERM();

        return 0;
}
