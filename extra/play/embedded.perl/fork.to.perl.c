#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <EXTERN.h>
#include <perl.h>

static PerlInterpreter *my_perl;

static char *embeded[] = { "", "-e", "0" };
static char *p2code = "use feature 'say'; $a = 34; $b = 12; $a = $a + $b;";
static char *scode = "$px = 0; $px += $_ for (1 .. 10); $px";

static void initPerl(void)
{
        PERL_SYS_INIT3(NULL, NULL, NULL);

        my_perl = perl_alloc();
        perl_construct(my_perl);

        perl_parse(my_perl, NULL, 3, embeded, NULL);
        PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
        perl_run(my_perl);
}

static void runPerl(char *perlCode, char **result)
{
        SV *scode = eval_pv(perlCode, TRUE);
        *result = SvPV_nolen(scode);
}

static void destroyPerl(void)
{
        perl_destruct(my_perl);
        perl_free(my_perl);
        PERL_SYS_TERM();
}

int main(int argc, char *argv[], char *env[])
{
        int status;
        pid_t cpid, w;
        int pipefd[2];
        char buf[1024];
        char *result;

        if (pipe(pipefd) == -1) {
                exit(EXIT_FAILURE);
        }
        initPerl();

        cpid = fork();

        if (cpid == -1) {
                exit(EXIT_FAILURE);
        }

        if (cpid == 0) {
                int nread;

                memset(buf, 0, 1024);

                nread = read(pipefd[0], &buf, 1024);
                runPerl(buf, &result);

                fprintf(stderr, "# (child) '%s'\n", result);
                /* write the result to the parent */
                write(pipefd[1], result, strlen(result));

                close(pipefd[1]);
                close(pipefd[0]);
        } else {
                int nread;
                char lbuf[1024];
                memset(lbuf, 0, 1024);

                write(pipefd[1], scode, strlen(scode));
                do {
                        w = waitpid(cpid, &status, WUNTRACED | WCONTINUED);
                        if (WIFEXITED(status)) {
                                fprintf(stderr, "# child exited\n");
                                nread = read(pipefd[0], &lbuf, 1024);
                                if (nread > 0) {
                                        fprintf(stderr, "# got from kid: '%s'\n", lbuf);
                                }

                        } else if (WIFSIGNALED(status)) {
                                fprintf(stderr, "# child signaled\n");
                        } else if (WIFSTOPPED(status)) {
                                fprintf(stderr, "# child stopped\n");
                        } else if (WIFCONTINUED(status)) {
                                fprintf(stderr, "# child cont\n");
                        }

                } while (!WIFEXITED(status) && !WIFSIGNALED(status));

                close(pipefd[0]);
                close(pipefd[1]);
                destroyPerl();
        }
        return 0;
}
