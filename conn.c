#include "common.h"
#include "xutil.h"
#include "conn.h"
#include "eperl.h"
#include "parse_cmd.h"

static const char *banner_req = "InMemoryDataBase. Type help for commands\r\n";
extern hash_internal_t **htable;
extern pthread_mutex_t lock_table;

extern unsigned int c_htable_contents(hash_internal_t *hash);

static char *client_commands[] = {
        [COMMAND_STORE]         = "store",
        [COMMAND_GET]           = "get",
        [COMMAND_LPUSH]         = "lpush",
	[COMMAND_LKEYS]		= "list",
	[COMMAND_CKEYS]		= "count",
        [COMMAND_LPOP]          = "lpop",
        [COMMAND_INCR]          = "incr",
        [COMMAND_SCRIPT]        = "script",
        [COMMAND_PING]          = "ping",
        [COMMAND_HELP]          = "help",
};
#define NR_CLIENT_CMDS  (sizeof(client_commands) / sizeof(client_commands[0]))

static char *reply_msg[] = {
        [COMMAND_UNKNOWN]       = "-ERR Command unknown",
        [COMMAND_INVALID]       = "-ERR Invalid command usage",
        [COMMAND_INVALID_REQ]   = "-ERR Invalid request",
        [COMMAND_INTERPRET_ERR] = "-ERR Intepreter error",
        [COMMAND_NOT_AVAILABLE] = "-ERR Unimplemented",
        [COMMAND_ERR]           = "-ERR",
        [COMMAND_PONG]          = "+PONG",
        [COMMAND_OK]            = "+OK",
};
#define NR_REPLY_CLIENT (sizeof(reply_client) / sizeof(reply_client[0]))        


void daemonize(void)
{
        pid_t pid, sid;

        if (getppid() == 1) {
                return;
        }
        pid = fork();
        switch (pid) {
                case -1:
                        FATAL("Could not fork: %s\n", strerror(errno));
                        break;
                case 0:
                        break;
                default:
                        exit(0);
        }
        if ((sid = setsid()) < 0) {
                FATAL("Could not set setsid: %s\n", strerror(errno));
        }

        if (chdir("/") < 0) {
                FATAL("Could not chdir to '/': %s\n", strerror(errno));
        }
        /* tie 0, 1 and 2 to /dev/null */
        freopen("/dev/null", "r", stdin);
        freopen("/dev/null", "w", stdout);
        freopen("/dev/null", "w", stderr);
}

int listento(int port)
{
        struct sockaddr_in srv_sock;        
        int s, y = 1;
        int ret = -1;

        if ((s = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
                LOG("Could not create socketfd: %s\n", strerror(errno));
                return ret;
        }
        
        if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &y, sizeof(y)) == -1) {
                LOG("setsockopt: %s\n", strerror(errno));
                close(s);
                return ret;
        }

        memset(&srv_sock, 0, sizeof(srv_sock));
        srv_sock.sin_port = htons(port);
        srv_sock.sin_family = AF_INET;

        if (bind(s, (struct sockaddr *) &srv_sock, sizeof(srv_sock)) == -1) {
                LOG("Could not bind: %s\n", strerror(errno));
                close(s);
                return ret;
        }

        listen(s, 10);
        LOG(">> accepting connections on port %d\n", port);

        return s;
}

static int unix_sock_init(struct sockaddr_un *sock)
{
        int s;

        if ((s = socket(PF_UNIX, SOCK_STREAM, 0)) < 0) {
                FATAL("# socket(): '%s'\n", strerror(errno));
        }


        memset(sock, 0, sizeof(sock));
        sock->sun_family = AF_UNIX;
        strncpy(sock->sun_path, PERL_SOCK_PATH, sizeof(PERL_SOCK_PATH));

        return s;
}

int bind_unix_sock(void)
{
        int s; 
        struct sockaddr_un sock;

        unlink(PERL_SOCK_PATH);

        s = unix_sock_init(&sock);

        if (bind(s, (struct sockaddr *) &sock, sizeof(struct sockaddr_un)) < 0) {
                FATAL("# bind(): '%s'\n", strerror(errno));
        }

        listen(s, 10);

        return s;
}


static int check_valid_cmd(char *str)
{
        int j;
        int lencmd = xstrlen(str);

        if (!lencmd) {
                return 0;
        }

        for (j = 0; j < NR_CLIENT_CMDS; j++) {
                if (!strcasecmp(str, client_commands[j])) {
                        DEBUG("# check_valid_cmd(): cmd found as '%s'\n", client_commands[j]);
                        return 1;
                }
        }
        return 0;
}

static command_client_t get_client_cmd(char *str)
{
        int j;
        int lencmd = xstrlen(str);

        if (!lencmd) {
                return -1;
        }

        for (j = 0; j < NR_CLIENT_CMDS; j++) {
                if (!strcasecmp(str, client_commands[j])) {
                        return j;
                }
        }
        return 0;
}

#define SMALLBUF	64

static int check_req_is_valid(char *buf)
{
        char *str = buf;
#if 0
        char *req_buf = NULL;
#endif
        int reqlen = 0;

        /* 
         * we pass this static array so we can 
         * use it to write pass the ret of 
         * get_first_val address
         */
        char req_buf[SMALLBUF];
        memset(req_buf, 0, SMALLBUF);

        get_first_val(req_buf, str, '!');

        if (req_buf == NULL) {
                LOG("# invalid command supplied. Could not found LENGTH_OF_REQUEST\n");
                return 0;
        }

        reqlen = atoi(req_buf);
        DEBUG("# total length of the request should be '%d'\n", reqlen);

        return (reqlen > 0) ? reqlen : 0;
}


static void sreplyToClient(int sock, command_reply_t type)
{
        char *toSend;
        int rlen;

        rlen = asprintf(&toSend, "%s\r\n", reply_msg[type]);

        if (rlen > 0) {
                reply(sock, toSend);
                xfree(toSend);
        }
}

static void replyToClient(int sock, command_reply_t type, const char *str)
{
        char *toSend;
        int rlen = 0;

        switch (type) {
                case COMMAND_NOT_AVAILABLE:
                case COMMAND_ERR:
                case COMMAND_UNKNOWN:
                case COMMAND_INVALID:
                case COMMAND_INTERPRET_ERR:
                case COMMAND_INVALID_REQ:
                        rlen = asprintf(&toSend, "%s %s\r\n", reply_msg[type], str);
                break;
                case COMMAND_REPLY:
                        rlen = asprintf(&toSend, "%s\r\n", str);
                break;
                case COMMAND_REPLY_BULK:
                        /* FIXME ? */
                        rlen = asprintf(&toSend, "%s\r\n", str);
                break;
                case COMMAND_PONG:
                case COMMAND_OK:
                        rlen = asprintf(&toSend, "%s\r\n", reply_msg[type]);
                break;
                        
        }

        if (rlen > 0) {
                reply(sock, toSend);
                xfree(toSend);
        }

}

/*
 * caller is response for free'ing it up
 */

static int parse_client_req(thread_data_t *t, int reqlen, char *buf)
{
        char *str = buf;
        char **req = NULL;
        int cmd_tokens = 0;
        client_cmd_request_t *creq = NULL;


        req = split_delim(str, DELIMITER, &cmd_tokens);
        if (!req || !*req) {
                return 0;
        }

        if (cmd_tokens != -1) {

                /* second is the command */
                if (!check_valid_cmd(req[1])) {
                        LOG("# invalid command '%s'\n", req[1]);
                        free_delim(req, cmd_tokens);
                        return 0;
                }

                command_client_t cmd = get_client_cmd(req[1]);
                DEBUG("# command given '%s'\n", client_commands[cmd]);

                creq = xmalloc(sizeof(*creq));
                memset(creq, 0, sizeof(*creq));

                /* the rest are the arguments for the command */
                int i, j = 2;

                switch (cmd) {
                        case COMMAND_HELP:
                                creq->cmd = cmd;
                        break;
                        case COMMAND_PING:
                                creq->cmd = cmd;
                        break;
                        case COMMAND_SCRIPT:
                                creq->cmd = cmd;
                                creq->key = xstrdup(req[j]);
                        break;
                        case COMMAND_INCR:
                                creq->cmd = cmd;
                                creq->key = xstrdup(req[j]);
                        break;
                        case COMMAND_GET:
                                creq->cmd = cmd;
                                creq->key = xstrdup(req[j]);
                        break;
			case COMMAND_LKEYS:
				creq->cmd = cmd;
			break;
			case COMMAND_CKEYS:
				creq->cmd = cmd;
			break;
                        case COMMAND_LPOP:
                                creq->cmd = cmd;
                                creq->key = xstrdup(req[j]);
                        break;
                        case COMMAND_LPUSH:
                        case COMMAND_STORE:

                                creq->cmd = cmd;
                                creq->key = xstrdup(req[j]);

                                /* advance to values */
                                j++;
                                if (cmd_tokens - j <= 0) {
                                        LOG("# invalid number of entries found\n");
                                        xfree(creq);
                                        free_delim(req, cmd_tokens);
                                        return 0;
                                }

                                DEBUG("# should allocate %d entries\n", cmd_tokens - j);
                                creq->values = xcalloc(cmd_tokens - j, sizeof(char *));

                                for (i = 0; j < cmd_tokens; j++, i++) {
                                        DEBUG("# arguments given: '%s'\n", req[j]);
                                        creq->values[i] = xstrdup(req[j]);
                                }

                                creq->nvalues = i;
                                creq->values[i++] = '\0';

                        break;
                        default:
                                return 0;
                        break;
                }
        }
        DEBUG("# parse_client_req() end\n");
        free_delim(req, cmd_tokens);

        /* save it */
        t->req = creq;
        return 1;
}

void free_client(thread_data_t *t)
{       
        int j;

        if (t->req) {
                if (t->req->key) {
                        xfree(t->req->key);
                }
                for (j = 0; j < t->req->nvalues; j++) {
                        if (t->req->values[j]) {
                                xfree(t->req->values[j]);
                        }
                }
                xfree(t->req);
        }
}


int connect_unix_sock(void)
{
        int s;
        struct sockaddr_un saddr;

        s = unix_sock_init(&saddr);
        
        if (connect(s, (struct sockaddr *) &saddr, sizeof(struct sockaddr_un)) == -1) {
                FATAL("# connect(): '%s'n", strerror(errno));
        }

        return s;
}

/*
 * caller is response for freeing it up
 *
 */
static int parse_script(char *code, char **result)
{
        int nwrite, len, nread = 0;
        char buf[BUFLEN];

        len = xstrlen(code);
        nwrite = 0;

        DEBUG("# parse_script()\n");
        int s = connect_unix_sock();

        /* write to children the script */
        nwrite = send(s, code, len, 0);

        if (nwrite <= 0) {
                LOG("# parse_script(): failed to send data to child\n");
                goto out;
        }

        if (len == nwrite) {
                DEBUG("# parse_script(): waiting for client to respond\n");
                memset(buf, 0, BUFLEN);

                /* block in read !!! */
                nread = read(s, &buf, BUFLEN);
                if (nread > 0) {
                        DEBUG("# got something from perl interpreter: '%s'\n", buf);
                        char *ret = xmalloc(nread + 1);
                        memset(ret, 0, nread + 1);

                        pthread_mutex_lock(&lock_table);
                        memcpy(ret, buf, nread);
                        *result = ret;
                        pthread_mutex_unlock(&lock_table);

                } else {
                        /* perl didn't return anything :-( */
                        DEBUG("# parse_script(): perl didn't return anything. probably it exited\n");
                }
        }
out:
        close(s);

        return nread > 0 ? nread : 0;
}

static void help_usage(int fd)
{
        int j;
        char buf[1024];
        memset(buf, 0, 1024);

        for (j = 0; j < NR_CLIENT_CMDS; j++) {
                if (j == COMMAND_STORE) {
                        sprintf(buf, "%s \t<key> <value>", client_commands[j]);
                } else if (j == COMMAND_GET) {
                        sprintf(buf, "%s \t<key>", client_commands[j]);
                } else if (j == COMMAND_PING) {
                        sprintf(buf, "%s", client_commands[j]);
                } else if (j == COMMAND_HELP) {
                        sprintf(buf, "%s", client_commands[j]);
                } else if (j == COMMAND_SCRIPT) {
                        sprintf(buf, "%s \t<script>", client_commands[j]);
                } else if (j == COMMAND_LPOP) {
                        sprintf(buf, "%s \t<value>", client_commands[j]);
                } else if (j == COMMAND_LPUSH) {
                        sprintf(buf, "%s \t<value>", client_commands[j]);
                } else {
                        sprintf(buf, "%s \t<key>", client_commands[j]);
                }
                replyToClient(fd, COMMAND_REPLY, buf);
        }

}

static int process_client_cmd(thread_data_t *t)
{
        int j, len;
        int nvalues = t->req->nvalues;
        char *val;
        client_cmd_request_t *command;
	hash_internal_t *hash = *htable;
	hash_entry_t *he;
	unsigned int cnt = 0;
	char *buf = NULL;

        command = t->req;

        if (command->cmd == -1) {
                LOG("# invalid command\n");
                return 0;
        }
	len = 0;

        switch (command->cmd) {
                case COMMAND_HELP:
                        help_usage(t->sock);
                break;
                case COMMAND_PING:
                        sreplyToClient(t->sock, COMMAND_PONG);
                break;
                case COMMAND_STORE:
                        DEBUG("*** (COMMAND_STORE) key: '%s'\n", t->req->key);
                        pthread_mutex_lock(&lock_table);
                        for (j = 0; j < nvalues; j++) {
                                hash_insert(htable, t->req->key, t->req->values[j]);
                                DEBUG("# added value: '%s'\n", t->req->values[j]);
                        }
                        sreplyToClient(t->sock, COMMAND_OK);
                        pthread_mutex_unlock(&lock_table);
                break;
                case COMMAND_GET:
                        DEBUG("*** (COMMAND_GET) key: '%s'\n", t->req->key);
                        pthread_mutex_lock(&lock_table);
                        val = (char *) hash_lookup(htable, t->req->key);
                        if (!val) {
                                LOG("# no such key: '%s'\n", t->req->key);
                                replyToClient(t->sock, COMMAND_REPLY, "-1");
				pthread_mutex_unlock(&lock_table);
				return 0;
                        }
                        DEBUG("# returning '%s' to client\n", val);
                        replyToClient(t->sock, COMMAND_REPLY, val);
                        pthread_mutex_unlock(&lock_table);
                break;
		case COMMAND_LKEYS:
                        DEBUG("*** (COMMAND_LKEYS)\n");
			char *buflen = NULL;

			pthread_mutex_lock(&lock_table);
			cnt = c_htable_contents(hash);

			asprintf(&buflen, "%u", cnt);
			/* send first how many entries will there be */
			replyToClient(t->sock, COMMAND_REPLY, buflen);
			if (cnt) {
				for (j = 0; j < hash->n_buckets; j++) {
					list_for_each_entry(he, &hash->buckets[j], list) {

						if (he->key) {
							asprintf(&buf, "%s", (char *) he->key);
							replyToClient(t->sock, COMMAND_REPLY, buf);
						}

					}
				}
			}
			pthread_mutex_unlock(&lock_table);

			if (buf)
				xfree(buf);
			if (buflen)
				xfree(buflen);
		break;
		case COMMAND_CKEYS:
                        DEBUG("*** (COMMAND_CKEYS)\n");

			pthread_mutex_lock(&lock_table);
			cnt = c_htable_contents(hash);
			pthread_mutex_unlock(&lock_table);

			len = asprintf(&buf, "%u", cnt);
			replyToClient(t->sock, COMMAND_REPLY, buf);
			xfree(buf);
		break;
                case COMMAND_INCR:
                        DEBUG("*** (COMMAND_INCR) key:'%s'\n", t->req->key);
                        pthread_mutex_lock(&lock_table);
                        val = (char *) hash_lookup(htable, t->req->key);
                        if (!val) {
                                LOG("# no such key: '%s'\n", t->req->key);
                                replyToClient(t->sock, COMMAND_ERR, "Key not found");
				pthread_mutex_unlock(&lock_table);
                                return 0;
                        }
                        long int incr = char2lint(val);
                        if (incr == -1) {
                                replyToClient(t->sock, COMMAND_ERR, "-1");
                                return 0;
                        }
                        incr++;  

                        len = asprintf(&buf, "%ld", incr);
                        if (len < 0) {

                                if (buf) {
                                        xfree(buf);
                                }

                                LOG("# parse_client_req(): failed to increment key: '%s'\n", t->req->key);
                                pthread_mutex_unlock(&lock_table);
                        }

                        hash_insert(htable, t->req->key, buf);
                        replyToClient(t->sock, COMMAND_REPLY, buf);
                        pthread_mutex_unlock(&lock_table);

                        /* FIXME: leaking here */
                break;
                case COMMAND_SCRIPT:
                        DEBUG("*** (COMMAND_SCRIPT)\n");
                        const char invalid[] = "perl terminated abruptly. Possible invalid script given";
                        char *sbuf = NULL;

                        /* 
                         * give the child the script to parse it
                         */
                        int r = parse_script(t->req->key, &sbuf);
                        if (r > 0) {
                                DEBUG("# got data from perl: '%s'\n", sbuf);
                                replyToClient(t->sock, COMMAND_REPLY, sbuf);
                        } else {
                                DEBUG("# parse_client_req():  failed to receive data from perl\n");
                                replyToClient(t->sock, COMMAND_ERR, invalid);
                        }

                        if (sbuf)
                                xfree(sbuf);
                break;
                case COMMAND_LPUSH:
                case COMMAND_LPOP:
                        sreplyToClient(t->sock, COMMAND_NOT_AVAILABLE);
                break;
                default:
                        sreplyToClient(t->sock, COMMAND_INVALID);
                break;
        }

        return 1;
}

/*
 * conn_work blocks in read 
 */

void conn_work(thread_data_t *t)
{
        char buf[BUFLEN];

        int nread;
        int reqlen;
        reply(t->sock, banner_req);

read_more:
        memset(buf, 0, BUFLEN);
        while ((nread = read(t->sock, &buf, BUFLEN)) > 0) {

                DEBUG(">> processing  '%d' bytes :'%s'\n", nread, buf);

                if ((reqlen = check_req_is_valid(buf)) < 0) {
                        LOG("# client send invalid request\n");
                        sreplyToClient(t->sock, COMMAND_INVALID_REQ);
                }

                if (!parse_client_req(t, reqlen, buf)) {
                        LOG("# failed to parse data\n");
                        sreplyToClient(t->sock, COMMAND_INVALID_REQ);
                        goto read_more;
                }

                if (t->req) {

                        DEBUG("# processing request...\n");
                        process_client_cmd(t);
                        goto read_more;
                }
        }

        if (nread < 0 && errno == EINTR) {
                DEBUG("# read_more\n");
                goto read_more;
        }

        if (nread <= 0) {
                DEBUG("# client closed connection\n");
                close(t->sock);
        }
}
