-   [NAME](#NAME)
-   [SYNOPSIS](#SYNOPSIS)
-   [DESCRIPTION](#DESCRIPTION)
-   [OPTIONS](#OPTIONS)
-   [PROTOCOL SPECIFICATION](#PROTOCOL-SPECIFICATION)
-   [RE-COMPILING](#RE-COMPILING)
-   [BUGS](#BUGS)
-   [AUTHORS](#AUTHORS)
-   [SEE ALSO](#SEE-ALSO)

NAME
====

IMDB

SYNOPSIS
========

**imdb** [OPTIONS]

DESCRIPTION
===========

Simple in memory database. IMDB uses a hash table to store *key*,
*value* pairs.

OPTIONS
=======

-d  
do not fork

-v  
run with verbose mode, implies -d

-p \<port\>  
specify other port to bind to

-h  
help page

-V  
display version

PROTOCOL SPECIFICATION
======================

REQUEST  
depending on the type of *COMMAND* optional attributes might be
required.

`!LENGTH_OF_REQUEST!COMMAND![<OPTIONAL_ATTR1>]![<OPTIONAL_ATTR2>]\r\n`

RESPONSE  
a response might be either a value (*VALUE*) an acknowledge (*+PONG* or
*+OK*) or a error (*-1*).

`[<-1|+PONG|+OK|VALUE>]\r\n`

COMMANDS  
Some basic commands are available:

**ping**  
send a PING request to the server.

**store** \<mykey\> \<myvalue\>  
store *myvalue* under *mykey*

**get** \<key\>  
retrieve a value for key *key*

**incr** \<key\>  
increment and retrieve contents of key *key* (given that the content of
the key is an integer).

**script**  
invoking perl is done by using *script* command:

**NOTE**: make sure you end your script with a return other, otherwise
you will not retrieve the contents of the *evaluated* perl script.

`{ $sx = 100; $sp = 23; $sp *= $sx * $_ for (1 .. 100); $sp }`

**list**  
iterate and return all available keys in the database.

**NOTE:** while this command is desirable when connections are coming
from localhost, doing so for all connections might *not* be desirable.

**count**  
count all keys in the database.

RE-COMPILING
============

*imdb* requires *perl* development libraries to compile succesfully.
Under a GNU/Linux machine this might be equivalent to *libperl-dev*
library.

Normal way  
You can re-compile IMDB by issuing a \`\`make'' in the root source
directory.

Compiling on a 64-bit machine a 32 bit version:  
Download *perlbrew*:

`$ curl -kL http://install.perlbrew.pl | bash`

Init *perlbrew*:

`$ source ~/perl5/perlbrew/etc/SHELL`

where *SHELL* is the shell you're currently running (i.e.: *bash*,
*zsh*).

Install a 32-bit perl version:

`$ perlbrew install perl-5.14.3 -Accflags='-m32 -march=i686' -Aldflags='-m32 -march=i686' -Alddlflags='-shared -m32 -march=i686'`

Switch to it:

`$ perlbrew use perl-5.14.3`

Then you can compile *imdb*:

Compiling:  
`$ make`

**NOTE:** in a new environment you'll need to switch to the 32 bit perl
version, or you can add the `$ source ~/perl5/perlbrew/etc/SHELL` to
your SHELL *rc configuration file*.

BUGS
====

Probably a lot.

AUTHORS
=======

This manual page has been written by Marius Vlad (mv@sec.uni-passau.de).

SEE ALSO
========

[http://redis.io](http://redis.io)