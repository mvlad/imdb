#ifndef __E_PERL_H
#define __E_PERL_H

#include <EXTERN.h>
#include <perl.h>

#define PERL_SOCK_PATH   "/tmp/sock"

extern void initPerl(int argc, char *argv[], char *env[]);
extern void runAsPerl(char *ecode, char **result);
extern void destroyPerl(void);
extern void bootstrapPerl(int p_sock);

#endif
