#include "common.h"
#include "xutil.h"
#include "conn.h"
#include "htable.h"
#include "eperl.h"
#include "timer.h"
#include "xutil.h"

LIST_HEAD(timers);
LIST_HEAD(running_threads);

static char *version = XSTR(BUILDVERSION);
unsigned int debug = 1;
unsigned int run_as_daemon = 1;
unsigned int port = 3769;
unsigned int visor_loop_epoch = 0;
struct timeval ts_start;

hash_internal_t **htable;
pthread_mutex_t lock_table;

pthread_mutex_t lock_runnable;


static rthread_t *init_thread(pid_t pid)
{

        rthread_t *thr = xmalloc(sizeof(struct rthread));
        memset(thr, 0, sizeof(struct rthread));

        thr->thread = pid;
        INIT_LIST_HEAD(&thr->list);

        return thr;
}

static void add_thread_to_list(rthread_t *thr)
{
        list_add_tail(&thr->list, &running_threads);
}

static unsigned int
djb_hash(char *str)
{
        unsigned int h0 = 5381;
        while (*str) {
                h0 = ((h0 << 5) + h0) + (unsigned char )(*str++);
        }
        return h0;
}

static void key_val_xfree(void *key, void *val)
{
        xfree(key);
        xfree(val);
}

void list_htable_contents(hash_internal_t *hash)
{
        int j;
        hash_entry_t *he;

        for (j = 0; j < hash->n_buckets; j++) {
                list_for_each_entry(he, &hash->buckets[j], list) {
                        if (he->key) {
                                LOG("# (entry: %d) found key: '%s' val: '%s'\n", j, (char *) he->key, (char *) he->val);
                        }
                }
        }

}

unsigned int c_htable_contents(hash_internal_t *hash)
{
        int j; unsigned int i = 0;
        hash_entry_t *he;

        for (j = 0; j < hash->n_buckets; j++) {
                list_for_each_entry(he, &hash->buckets[j], list) {
                        if (he->key) {
                                i++;
                        }
                }
        }
        return i;
}

static pid_t spawn_child(int p_sock)
{
        int cpid;

        cpid = fork();
        if (cpid == -1) {
                FATAL("# could not fork\n");
        }
        if (cpid == 0) {
                /* wait for input */
                bootstrapPerl(p_sock); 
        }
        return cpid;
}

static void *thread_perl(void *args)
{
        int status, psock;
        pid_t w, cpid;
        thread_intern_t tintern;

        tintern = *(struct thread_intern *) args;
        psock = tintern.psock;

        LOG("# spawned thread_perl\n");

        /* init perl */
        initPerl(0, NULL, NULL);

spawn_another:
        cpid = spawn_child(psock);

        LOG(">>> spawned child with pid: '%d'\n", cpid);

        do {
                w = waitpid(cpid, &status, WUNTRACED | WCONTINUED);
                if (w == -1) {
                        FATAL("# waitpid() \n");
                }

                if (WIFEXITED(status) || WIFSIGNALED(status)) {

                        LOG("# child exited or signaled\n");
                        goto spawn_another;

                } else if (WIFSTOPPED(status)) {
                        LOG("# child stopped\n");
                } else if (WIFCONTINUED(status)) {
                        LOG("# child cont\n");
                }

        } while (!WIFEXITED(status) && !WIFSIGNALED(status));

        return NULL;
}

static void *thread_supervisor(void *args)
{
        int r; unsigned int timeout;
        struct timeval tv;
        unsigned int cnt = 0;

        LOG("# spawned thread_supervisor\n");
        for (;;) {

                timeout = compute_timeout();

                tv.tv_sec        = timeout / 1000;
                tv.tv_usec       = (timeout - tv.tv_sec * 1000) * 1000;

                r = select(0, NULL, NULL, NULL, &tv);

                if (r == -1 && errno == EINTR) {
                        continue;
                }

                if (r == -1) {
                        return NULL;
                }

                do_timers();
                if (debug && !run_as_daemon) {
                        list_htable_contents(*htable);
                        if ((cnt = c_htable_contents(*htable)) > 0) {
                                LOG("# found '%u' keys in table\n", cnt);
                        }
                }

                visor_loop_epoch++;
        }
        return NULL;
}


static void *thread_main(void *args)
{
        int srv_sock, nr;
        thread_data_t t;
        socklen_t addrlen;
        struct sockaddr_in client;
        thread_intern_t tintern;

        tintern = *(thread_intern_t *) args;
        srv_sock        = tintern.sfd;
        nr              = tintern.nr;

        LOG("# spawned thread_main (%d)\n", nr);

        t.psock         = tintern.psock;
        t.thread_id     = pthread_self();
        t.thread_nr     = nr;

        pthread_mutex_init(&t.lock, NULL);

        t.req = NULL;
                
        rthread_t *thr = init_thread(syscall(SYS_gettid));
        pthread_mutex_lock(&lock_runnable);
        add_thread_to_list(thr);
        pthread_mutex_unlock(&lock_runnable);

        for (;;) {
                int r;
                fd_set rd;
                t.nfds = 0;

                FD_ZERO(&rd);
                FD_SET(srv_sock, &rd);

                t.nfds = MAX(t.nfds, srv_sock);

                r = pselect(t.nfds + 1, &rd, NULL, NULL, NULL, NULL);

                if (r == -1 && errno == EINTR) {
                        continue;
                }

                if (r == -1) {
                        FATAL("# pselect %s\n", strerror(errno));
                }

                if (FD_ISSET(srv_sock, &rd)) {

			addrlen = sizeof((struct sockaddr*) &client);
                        t.sock = accept(srv_sock, (struct sockaddr *) &client, &addrlen);

                        if (t.sock == -1) {
                                if (addrlen < 0) {
                                        LOG("# addrlen is negative: '%d'\n", (int ) addrlen);
                                }

                                if ((struct sockaddr *) &client == NULL) {
                                        LOG("# client sockaddr is invalid\n");
                                }

                                FATAL("# thread_main: accept() '%s'\n", strerror(errno));
                                return NULL;
                        }

                        /* add the accepted sock to read list */
                        FD_SET(t.sock, &rd);
                        t.nfds = MAX(t.nfds, t.sock);
                }

                if (FD_ISSET(t.sock, &rd)) {
                        conn_work(&t);
                }
        }
        return NULL;
}

static void sigint_handler(int sig, siginfo_t *siginfo, void *data)
{
        LOG("# received SIGINT\n");

        destroyPerl();
        pthread_mutex_destroy(&lock_table);

        exit(EXIT_SUCCESS);
}


static void 
usage(void)
{
    fprintf(stderr, "In Memory Database\n");
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t-d no not fork\n"); 
    fprintf(stderr, "\t-v verbose, implies -d\n");
    fprintf(stderr, "\t-p <port>\n");
    fprintf(stderr, "\t-h this help\n");
    fprintf(stderr, "\t-V show current version\n");
    exit(EXIT_FAILURE);
}

static void parse_argv(int argc, char *argv[], char *env[])
{
    int c, i;


    while ((c = getopt(argc, argv, "dhvVp:")) != -1) {
        switch(c) {
            /* be highly verbose */
            case 'v':
                debug = 1;
                run_as_daemon = 0;
            break;
            /* do no daemonize */
            case 'd':
                run_as_daemon = 0;
            break;
            case 'p':
                port = atoi(optarg); 
            break;
            case 'V':
                fprintf(stderr, "%s\n", version);
                exit(EXIT_SUCCESS);
            break;
            case 'h':
                usage();
            break;
            default:
                usage();
            break;
        }
    }
    for (i = optind; i < argc; i++)
        fprintf (stderr, "Non-option argument %s\n", argv[i]);

}


int main(int argc, char* argv[], char *env[])
{

        int j, sfd, pfd;

        pthread_t *threads;
        pthread_t perl_thread;
        pthread_t visor_thread;

        struct timer *status_timer, *thread_timer;
        struct sigaction int_action;

        /* when did we start... */
        get_current_ts(&ts_start);

        /* parse args */
        parse_argv(argc, argv, env); 

        /* bind and list */
        sfd = listento(port);
        if (sfd == -1) {
                exit(EXIT_FAILURE);
        }

        /*
         * create a UNIX sock to communicate
         * with the child dealing with perl 
         */
        pfd = bind_unix_sock();

        if (run_as_daemon) {
                daemonize();
        }

        /* handle SIGINT */
        memset(&int_action, 0, sizeof(int_action));
        int_action.sa_flags = SA_SIGINFO;
        int_action.sa_sigaction = sigint_handler;
        sigaction(SIGINT, &int_action, NULL);

	signal(SIGPIPE, SIG_IGN);;

        /* init status timer cb */
        status_timer = create_timer(status_timer_cb, NULL);
        set_timer(status_timer, STATUS_INTERVAL);

        /* init thread status cb */
        thread_timer = create_timer(thread_timer_cb, NULL);
        set_timer(thread_timer, THREAD_STATUS_INTERVAL);

        /* 
         * init htable
         */
        htable = hash_create(N_BUCKETS, THREAD_COUNT, 
                            (k_hash_func) djb_hash, (k_compare_func) strncmp, 
                            (k_val_destroy_func) key_val_xfree);

        /* thread alloc */
        threads = xcalloc(THREAD_COUNT, sizeof(pthread_t));

        pthread_mutex_init(&lock_table, NULL);
        pthread_mutex_init(&lock_runnable, NULL);

        /* open log */
        openlog("imdb", LOG_NDELAY, LOG_DAEMON);

        /* thread data to pass to visor perl thread */
        thread_intern_t tdata;
        tdata.sfd       = sfd;
        tdata.psock     = pfd;

        /* perl thread to handle a child */
        pthread_create(&perl_thread, NULL, thread_perl, (void *) &tdata);

        /* do_timers visor */
        pthread_create(&visor_thread, NULL, thread_supervisor, NULL);

        /* thread data to pass to connect threads */
        thread_intern_t tsdata[THREAD_COUNT];
        /* children */
        for (j = 0; j < THREAD_COUNT; j++) {

                tsdata[j].sfd = sfd;
                tsdata[j].psock = pfd;
                tsdata[j].nr = j;

                pthread_create(&threads[j], NULL, thread_main, (void *) &tsdata[j]);
        }
        /* stupid */
        for (j = 0; j < THREAD_COUNT; j++) {
                pthread_join(threads[j], NULL);
        }

        pthread_mutex_destroy(&lock_table);
        pthread_mutex_destroy(&lock_runnable);

        return 0;
}
