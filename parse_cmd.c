#include "common.h"
#include "xutil.h"

static int count_delim(const char *str, char *delim)
{
        int cnt = 0;
        const char *ptr = str; 

        while (ptr && *ptr) {
                if (*delim == *ptr) {
                        cnt++;
                }
                ptr++;
        }
        return cnt;
}

static int strip_newln(char **val)
{
        char *str = *val;

        while (str && *str) {
                char *ptr = str;
                if (*ptr == '\r' && *(++ptr) == '\n') {
                        *str = '\0';
                        return 1;
                }
                str++;
        }
        return 0;
}


char *get_first_val(char *buf, const char *str, char delim)
{
        const char *p = str;
        int len = 0;

	char *bp = buf;

        if (*p != delim) {
                LOG("# invalid string passed. Could not find start '%x'\n", delim);
                return NULL;
        }
        
        p++;

	/* use a different method */
	const char *eptr = p;
	const char *end = NULL;
	
	while (eptr) {
#if 0
                if (*eptr == '\0')
                        LOG("# found NUL byte\n");
#endif
		if (*eptr == '!') {
			end = eptr;
			break;
		}
		eptr++;
	}

        if (end == NULL) {
                LOG("# invalid string passed. Could not find end delim '%c'\n", delim);
                return NULL;
        }

        len = end - p;
        if (len == 0) {
                LOG("# string length is 0.\n");        
                return NULL;
        }
        LOG("len has size: %d\n", len);

	while (len) {

		while (p && *p != delim && len > 0)  {
			LOG("# parsing '%c'\n", *p); 
			*bp++ = *p++;
			len--;
		}

		if (len == 0) {
			LOG("# found 0 len\n");
		}

		LOG("# len has '%d'\n", len);
	}
        return buf;
}

/*
 * caller must free it 
 */

char **split_delim(char *str, char *delim, int *nvals)
{
        char **ret;       
        int cnt, j, scnt, tlen;
        char *lwork;
        char *sptr; char *rptr;

        cnt = j = tlen = 0;

        /* allocate internaly */       
        lwork = xmalloc(strlen(str) + 1); 
        memset(lwork, 0, strlen(str) + 1);
        memcpy(lwork, str, strlen(str));

        cnt = count_delim(str, delim);

        if (cnt == 0) {
                LOG("# Invalid data received.\n");
                return NULL;
        }

        /* allocate for returning */
        ret = xcalloc(cnt, BUFLEN);

        /* save total tokens for return */
        scnt = cnt;

        rptr = lwork;
        char *p = strtok_r(rptr, delim, &sptr);

        j = 0; 
        while (p && cnt-- > 0) {
                if (strip_newln(&p)) {
                        DEBUG("# found new line\n");
                }
                ret[j++] = xstrdup(p);
                p = strtok_r(NULL, delim, &sptr);
        }

        xfree(lwork);

        *nvals = scnt;
        return ret;
}

void free_delim(char **xstr, int len)
{
        int j;

        for (j = 0; j < len; j++) {
                xfree(xstr[j]);
        }

        xfree(xstr);
}
