#include "common.h"
#include "timer.h"
#include "xutil.h"

extern struct list_head timers;
extern struct list_head running_threads;
extern struct timeval ts_start;

struct timer *create_timer(timer_cb cb, void *data)
{
        struct timer *tm = xmalloc(sizeof(struct timer));
        memset(tm, 0, sizeof(struct timer));

        tm->cb = cb;
        tm->data = data;

        INIT_LIST_HEAD(&tm->list);
        return tm;
}

void insert_timer(struct timer *tm)
{
       struct timer *pred; 
       struct timeval tv;

       tv.tv_sec        = tm->timeout / 1000;
       tv.tv_usec       = (tm->timeout - tv.tv_sec * 1000) * 1000;

       get_current_ts(&tm->tv);

       add_tv_to_tv(&tm->tv, &tv);

       if (list_empty(&timers)) {
               list_add_tail(&tm->list, &timers);
               return;
       }

       list_for_each_entry(pred, &timers, list) {
                if (tv_cmp(&tm->tv, &pred->tv) <= 0) {
                        list_add_tail(&tm->list, &pred->list);
                        return;
                }
       }

       list_add_tail(&tm->list, &timers);
}

void set_timer(struct timer *tm, unsigned int ms)
{
       if (ms == 0) {
               FATAL("Timer @ 0 ms.\n");
       }

       cancel_timer(tm);

       get_current_ts(&tm->set_at);
       tm->timeout = ms;

       insert_timer(tm);
}

void cancel_timer(struct timer *tm)
{
        if (!list_empty(&tm->list)) {
                list_del_init(&tm->list);
        }
}

void destroy_timer(struct timer *tm)
{
        cancel_timer(tm);
}

static int is_running(pid_t p)
{
        char buf[128];
        FILE *fin;

        memset(buf, 0 ,128);
        sprintf(buf, "/proc/%d/status", p);
        if ((fin = fopen(buf, "r")) == NULL) {
                return 0;
        }

        fclose(fin);
        return 1;
}

int thread_timer_cb(struct timer *tm, void *data)
{
        int j, cnt = 0;

        struct list_head *pos, *tmp;
        rthread_t *thr;

        list_for_each_safe(pos, tmp, &running_threads) {
                thr = list_entry(pos, struct rthread, list);

                if (!is_running(thr->thread)) {
                        LOG("# no thread running with pid: '%d'\n", thr->thread);
                        list_del(pos);
                        xfree(thr);
                } else {
                        cnt++;
                }
        }

        for (j = cnt; j < THREAD_COUNT; j++) {
                /* create thread here ? */
                LOG("# should spawn thread: '%d'\n", j);
        }

        return 1;        
}

int status_timer_cb(struct timer *tm, void *data)
{
        static char buf[512];
        char *vm_peak, *vm_size, *vm_rss;
        struct timeval now;
        char *p;
        FILE *fin;

        vm_size = vm_peak = vm_rss = NULL;

        if ((fin = fopen("/proc/self/status", "r")) == NULL)
                return -1;

        while (fgets(buf, 512, fin) != NULL) {
                if (strncmp(buf, "VmPeak:", strlen("VmPeak:")) == 0) {
                        p = buf + strlen("VmPeak:");
                        while (*p && isspace(*p))
                                p++;
                        vm_peak = xstrdup(p);
                        p = vm_peak;
                        while (*p && (*p != '\n'))
                                p++;
                        *p = 0;
                        continue;
                }
                if (strncmp(buf, "VmSize:", strlen("VmSize:")) == 0) {
                        p = buf + strlen("VmSize:");
                        while (*p && isspace(*p))
                                p++;
                        vm_size = xstrdup(p);
                        p = vm_size;
                        while (*p && (*p != '\n'))
                                p++;
                        *p = 0;
                        continue;
                }
                if (strncmp(buf, "VmRSS:", strlen("VmRSS:")) == 0) {
                        p = buf + strlen("VmRSS:");
                        while (*p && isspace(*p))
                                p++;
                        vm_rss = xstrdup(p);
                        p = vm_rss;
                        while (*p && (*p != '\n'))
                                p++;
                        *p = 0;
                        continue;
                }
        }

        fclose(fin);
        get_current_ts(&now);

        unsigned int diff = (unsigned int) now.tv_sec - ts_start.tv_sec;
        syslog(LOG_NOTICE, "pid %u, uptime: %s (%u) vm_size: %s, vm_rss: %s, vm_peak: %s",
                        getpid(), xtime_readable(diff), diff,
                        vm_size, vm_rss, vm_peak);
        xfree(vm_peak);
        xfree(vm_rss);
        xfree(vm_size);
        return 1;
}

inline unsigned int compute_timeout(void)
{
        struct timer *tm;
        struct timeval current_time;
        unsigned int timeout;

        get_current_ts(&current_time);

        if (list_empty(&timers)) {
                return -1;
        }

        tm = list_entry(timers.next, struct timer, list);

        if (tv_cmp(&tm->tv, &current_time) <= 0) {
                LOG("# WARNING: timer already expired!!!\n");
                return 0;
        }

        timeout = tv_diff(&tm->tv, &current_time);
        if (timeout == 0) {
                DEBUG("# 0 timeout\n");
                return 1;
        }

        return timeout;
}

void do_timers(void)
{
        struct timer *tm;
        struct timeval current_time;

        get_current_ts(&current_time);

        while (1) {
                if (list_empty(&timers)) {
                        break;
                }

                tm = list_entry(timers.next, struct timer, list);
                if (tv_cmp(&tm->tv, &current_time) <= 0) {
#if 0
                        long diff_ms = tv_diff(&current_time, &tm->set_at);
                        diff_ms -= tm->timeout;
                        LOG("# Running timer %p at %u:%u. Set at %u:%u. For %u ms. Error: %ld ms\n", 
                                tm, (unsigned int  ) current_time.tv_sec, (unsigned int) current_time.tv_usec,
                                (unsigned int) tm->set_at.tv_sec, (unsigned int) tm->set_at.tv_usec,
                                (unsigned int) tm->timeout, diff_ms);
#endif
                        list_del_init(&tm->list);
                        if (tm->cb(tm, tm->data)) {
                                if (tm->timeout == 0) {
                                        LOG("# zero timeout ?\n");
                                }

                                get_current_ts(&tm->set_at);
                                /* re-insert it back */
                                insert_timer(tm);
                        }
                } else {
                        /* expired timers */
                        break;
                }
        }
}
