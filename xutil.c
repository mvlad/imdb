#include "common.h"
#include "xutil.h"
#include "conn.h"

#define BUF_SIZE 1024
static char xutil_buf[BUF_SIZE];

void *xmalloc(size_t size)
{
        void *p = malloc(size);

        if (p == NULL) {
                FATAL("malloc failed @ %p\n", __builtin_return_address(0));
        }

        return p;
}

void *xcalloc(size_t nmemb, size_t size)
{
        void *p = malloc(size * nmemb);

        if (p == NULL) {
                FATAL("calloc failed @ %p\n", __builtin_return_address(0));
        }
        memset(p, 0, size * nmemb);

        return p;
}
void *xrealloc(void *ptr, size_t size)
{
        void *p = realloc(ptr, size);

        if (p == NULL) {
                FATAL("realloc failed @ %p\n", __builtin_return_address(0));
        }

        return p;
}

void xfree(void *ptr)
{
        if (ptr != NULL) {
                free(ptr);
        }
}

int xstrlen(const char *s)
{
        if (s == NULL) {
                return 0;
        } else {
                return strlen(s);
        }
}

char *xstrdup(const char *s)
{
        char *d;

        if (s) {
                d = strdup(s);
        } else {
                d = strdup("");
        }

        if (d == NULL) {
                FATAL("strdup failed @ %p\n", __builtin_return_address(0));
        }
        return d;
}

void xsetnonblocking(int fd)
{
        fcntl(fd, F_SETFL, O_NONBLOCK | fcntl(fd, F_GETFL));
}

ssize_t reply(int fd, const char *str)
{
        return sendto(fd, str, xstrlen(str), 0, NULL, 0); 
}

long int char2lint(char *val)
{
        long int ret = 0;
        char *ptr = val;

        while (ptr && *ptr != '\0') {
                int d = *ptr++ - '0';
                if (d < 0 || d > 9) {
                        return -1;
                }

                ret = 10 * ret - d;
        }
        return -ret;
}

char *
xtime_readable(unsigned int seconds)
{
        unsigned int days, hours, mins, secs;

        secs = seconds;

        days = secs / 86400;
        secs = secs % 86400;

        hours = secs / 3600;
        secs  = secs % 3600;

        mins  = secs / 60;
        secs  = secs % 60;

        if (seconds > 86400) {
                snprintf(xutil_buf, BUF_SIZE, "%u days, %u hrs, %u min, %u secs", days, hours, mins, secs);
        } else if (seconds > 3600) {
                snprintf(xutil_buf, BUF_SIZE, "%u hrs, %u min, %u secs", hours, mins, secs);
        } else if (seconds > 60) {
                snprintf(xutil_buf, BUF_SIZE, "%u min, %u secs", mins, secs);
        } else {
                snprintf(xutil_buf, BUF_SIZE, "%u sec", secs);
        }
        return xutil_buf;
} 
