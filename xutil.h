#ifndef __XUTIL_H
#define __XUTIL_H

#ifndef MAX
#define MAX(a,b)        (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b)        (((a) < (b)) ? (a) : (b))
#endif

extern void *xmalloc(size_t size);
extern void *xcalloc(size_t nmemb, size_t size);
extern void *xrealloc(void *ptr, size_t size);
extern void xfree(void *ptr);

extern char *xstrdup(const char *s);
extern int xstrlen(const char *s);

extern void xsetnonblocking(int fd);
extern ssize_t reply(int fd, const char *str);
extern long int char2lint(char *val);
extern char *xtime_readable(unsigned int secs);

#endif
