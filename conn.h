#ifndef __CONN_H
#define __CONN_H
#include "htable.h"


/*
 * Requests:
 * ---------
 *
 * not having a '{' means a normal command:
 * ``!LENGTH_OF_THIS_REQUEST!command!key!value\r\n''
 *
 * where command is one of the following:
 * - store
 * - get
 * - incr
 *
 * starting with '{' is a script comand:
 * ``!LENGHT_OF_THIS_REQUEST!{ print $_ * $_ for (1..100) }\r\n''
 *
 * Response:
 * --------
 *
 * for storing information / script execution
 *
 * - command was considered to be OK:
 *
 *  +OK
 *  '+OK\r\n'
 *  for a request like:
 *  ``!LENGTH_OF_THIS_REQUEST!store!animal!cat\r\n''
 *
 * - command was not considered to be OK:
 *
 *  -ERR Invalid command usage
 *  '-ERR Invalid command usage\r\n'
 *
 *  -ERR Unknown command 'command'
 *  '-ERR Unknown Command 'command'\r\n'
 *
 * for retriving information get/lpop
 *
 * '-1\r\n'         - key does not exists
 * 'value\r\n'      - key has ``value''
 */

#define DELIMITER       "!"
#define MESSAGE_START   DELIMITER
#define MESSAGE_END     "\r\n"
#define START_SCRIPT    "{"
#define END_SCRIPT      "}"
#define S_COMMAND       "*"

typedef enum {
        COMMAND_INVALID_REQ,            /* client sent malformed data */
        COMMAND_UNKNOWN,                /* command is not known */
        COMMAND_INTERPRET_ERR,          /* interpreter failed to parse */
        COMMAND_INVALID,                /* command known but wrong usage */
        COMMAND_ERR,                    /* generic error, for combining with custom message */
        COMMAND_REPLY,                  /* single reply */
        COMMAND_REPLY_BULK,             /* multiple replies values */
        COMMAND_PONG,                   /* reply to ping clients */
        COMMAND_NOT_AVAILABLE,          /* command not implemented */
        COMMAND_OK,                     /* for generic ack. i.e. when storing values */
} command_reply_t;

typedef enum {
        COMMAND_STORE,
        COMMAND_GET,
        COMMAND_LPUSH,
        COMMAND_LPOP,
        COMMAND_INCR,
        COMMAND_SCRIPT,
	COMMAND_LKEYS,
	COMMAND_CKEYS,
        COMMAND_HELP,
        COMMAND_PING,
} command_client_t;

typedef struct {
        command_client_t        cmd;
        char                    *key;
        unsigned int            nvalues;
        char                    **values;
} client_cmd_request_t;

typedef struct {
        pthread_t               thread_id;
        int                     thread_nr;
        int                     nfds;
        /* for talking with the client */
        int                     sock;
        /* for talking with a forked child */
        int                     psock;
        client_cmd_request_t    *req;
        /* lock this thread data */
        pthread_mutex_t         lock;
} thread_data_t;

extern int listento(int port);
extern int perl_sock_init(void);
extern void daemonize(void);
void conn_work(thread_data_t *t);
extern int bind_unix_sock(void);
extern int connect_unix_sock(void);

#endif
