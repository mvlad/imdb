#include "common.h"
#include "xutil.h"
#include <netdb.h>
#include <stdarg.h>

static const char *banner = "InMemoryDataBase. Type help for commands\r\n";

typedef enum {
        OK              = 0,
        ERR_CONNECT     = 1,
        ERR_WRONGFLAG   = 5,
        ERR_GENERIC     = 6,
        ERR_UNKNOWN     = 21,
        ERR_PROTO       = 25,
} generic_test_reply_t;

static char *err_replies[] = {
        [ERR_CONNECT]           = "Could not connect",
        [ERR_WRONGFLAG]         = "Invalid flag, or no flag found",
        [ERR_GENERIC]           = "Service lacks functionality",
        [ERR_UNKNOWN]           = "Unknown status",
        [ERR_PROTO]             = "Invalid proto",
};

typedef enum { COMMAND_PONG, COMMAND_OK } server_reply_t;

static char *server_replies[] = {
        [COMMAND_PONG]  = "+PONG",
        [COMMAND_OK]    = "+OK",
};

typedef enum { CMD_STORE, CMD_RETRIEVE, CMD_LKEYS, CMD_CKEYS, CMD_PING } cl_cmds_t;

static char *client_commands[] = {
        [CMD_STORE]         	= "store",
        [CMD_RETRIEVE]      	= "retrieve",
        [CMD_LKEYS]		= "list",
	[CMD_CKEYS]		= "count",
        [CMD_PING]          	= "ping",
};
#define NR_CMDS (sizeof(client_commands) / sizeof(client_commands[0]))

/* 
 * simple send
 *
 */
static ssize_t ssend(int fd, cl_cmds_t cmd)
{
        char *buf;
        int lstr, tot;
        ssize_t tosend;

        tot = lstr = 0;
        lstr = xstrlen(client_commands[cmd]);
        tot = 2 + lstr;

        asprintf(&buf, "!%d!%s\r\n", tot, client_commands[cmd]);

        tosend = sendto(fd, buf, xstrlen(buf), 0, NULL, 0); 
        xfree(buf);

        return (tosend > 0) ? tosend : 0;
}

/*
 * param send
 *
 */
static ssize_t lsend(int fd, cl_cmds_t cmd, char *key, ...) 
{
        va_list ap;
        char *buf;
        int tlen = 0;
        ssize_t tosend;

        va_start(ap, key);
        char *val = va_arg(ap, char *);

        switch (cmd) {
                case CMD_STORE:
                        tlen = xstrlen("store") + xstrlen(key) + xstrlen(val) + 2;
                        asprintf(&buf, "!%d!store!%s!%s\r\n", tlen, key, val);
                        break;
                case CMD_RETRIEVE:
                        tlen = xstrlen("get") + xstrlen(key) + 2;
                        asprintf(&buf, "!%d!get!%s\r\n", tlen, key);
                        break;
                default:
                        break;
        }

        tosend = sendto(fd, buf, xstrlen(buf), 0, NULL, 0); 
        va_end(ap);
        xfree(buf);

        return (tosend > 0) ? tosend : 0;
}

/*
 * send a PING cmd
 */

static int is_valid_srv(int s)
{
        int nread;
        ssize_t nsend;
        char buf[1024];

                
        nsend = ssend(s, CMD_PING);

        if (nsend > 0) {

                memset(buf, 0, 1024);
                nread = read(s, &buf, 1024);
                if (nread > 0) {
                        
                        char *sreply = server_replies[COMMAND_PONG];
                        int len = xstrlen(server_replies[COMMAND_PONG]);
                        if (!strncasecmp(buf, sreply, len)) {
                                return 1;
                        }
                }
        }
        return 0;
}

/* send a store cmd */

static int store_flag(int s, char *fid, char *flag)
{
        ssize_t nsend; 
        int nread;
        char buf[1024];

        nsend = lsend(s, CMD_STORE, fid, flag);

        if (nsend > 0) {
                memset(buf, 0, 1024);

                nread = read(s, &buf, 1024);
                if (nread > 0) {
                        char *sreply = server_replies[COMMAND_OK];
                        int len = xstrlen(server_replies[COMMAND_OK]);

                        if (!strncasecmp(buf, sreply, len)) {
                                return 1;
                        }
                }
        }
        fprintf(stderr, "# server responded '%s'\n", buf);

        return 0;
}

static int strip_newln(char **val)
{
        char *str = *val;

        while (str && *str) {
                char *ptr = str;
                if (*ptr == '\r' && *(++ptr) == '\n') {
                        *str = '\0';
                        return 1;
                }
                str++;
        }
        return 0;
}

/* send a get cmd */
static int retrieve_flag(int s, char *fid, char *res)
{
        ssize_t nsend; 
        int nread;
        char buf[1024];

        nsend = lsend(s, CMD_RETRIEVE, fid);

        if (nsend > 0) {
                memset(buf, 0, 1024);

                nread = read(s, &buf, 1024);
                if (nread > 0) {
                        char *ptr = buf;

                        strip_newln(&ptr);

                        if (!strncasecmp(ptr, res, xstrlen(ptr))) {
                                return 1;
                        }
                }
        }

        return 0;
}

static void usage(void)
{
        fprintf(stderr, "Test flag storing and retrieval\n");
        fprintf(stderr, "Usage:\n");
        fprintf(stderr, "-c [<store>|<retrieve>|<list>|<count>] -s <source-ip> -p <port> -i <flag-id> -f <flag>\n");
        exit(EXIT_FAILURE);
}

static int remoteConn(char *ip, char *port)
{
        int s, sfd;
        struct addrinfo hints;
        struct addrinfo *result, *rp;

        memset(&hints, 0, sizeof(struct addrinfo));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = 0;
        hints.ai_flags = 0;

        s = getaddrinfo(ip, port, &hints, &result);
        if (s != 0) {
                fprintf(stderr, "# getaddrinfo: '%s'\n", strerror(errno));
                exit(EXIT_FAILURE);
        }

        for (rp = result; rp != NULL; rp = rp->ai_next) {
                if ((sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol)) == -1) {
                        continue;
                }
                if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1) {
                        break;
                }
                close(sfd);
        }

        if (rp == NULL) {
                return -1;
        }
        freeaddrinfo(result);

        return sfd;
}

static cl_cmds_t get_command(char *str)
{
        int j;

        for (j = 0; j < NR_CMDS; j++) {
                if (!strcasecmp(str, client_commands[j])) {
                        return j;
                }
        }
        return -1;
}

static void readData(int sfd)
{
	char buf[1024];
	int pr;
	int r, sum;

	struct timespec ts;

	if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1) {
		exit(EXIT_FAILURE);
	}

	fcntl(sfd, F_SETFL, O_NONBLOCK | fcntl(sfd, F_GETFL));

	sum = 0;
	for (;;) {

		int nfds = 0;
		fd_set rfd;
		pr = 0;

		ts.tv_sec = 0;
		ts.tv_nsec = 100 * 1000000;

		FD_ZERO(&rfd);
		FD_SET(sfd, &rfd);

		nfds = nfds > sfd ? nfds : sfd;

		r = pselect(nfds + 1, &rfd, NULL, NULL, &ts, NULL);

		if (r == -1 && errno == EINTR) {
			continue;
		}
		if (r == -1) {
			exit(EXIT_FAILURE);
		}

		if (FD_ISSET(sfd, &rfd)) {

			memset(buf, 0, 1024);	
			while ((pr = read(sfd, &buf, 1024)) > 0) {

				if (pr > 0) {
					fprintf(stderr, "[+] server send: '%s'\n", buf);
				}
				sum += pr;
				fprintf(stderr, "# read has %d\n", sum);
			}

			if (pr < 0 && errno == EINTR) {
				/* go another loop */
				continue;
			}

			/* no more data on the sock */
			if (pr < 0 && errno == EAGAIN) {
				/* go another loop */
				continue;
			}

			/* no more data on the sock */
			if (pr <= 0 && errno == EWOULDBLOCK) {
				return;
			}

		} else {
			return;
		}
	}
}

int main(int argc, char *argv[])
{
        int c;
        char *ip, *flagid, *flag;
        char *comm, *port;
        int sfd;
        char buf[1024];

        ip = flagid = flag = comm = port = NULL;

        if (argc < 10) {
                usage();
        }

        while ((c = getopt(argc, argv, "c:s:p:i:f:")) != -1) {
                switch (c) {
                        case 'c':
                                comm = xstrdup(optarg);
                                break;
                        case 's':
                                ip = xstrdup(optarg);
                                break;
                        case 'p':
                                port = xstrdup(optarg);
                        break;
                        case 'i':
                                flagid = xstrdup(optarg);
                                break;
                        case 'f':
                                flag = xstrdup(optarg);
                                break;
                        default:
                                usage();
                                break;

                }
        }

        fprintf(stderr, "Command given: '%s'\n", comm);

        if ((sfd = remoteConn(ip, port)) == -1) {
                fprintf(stderr, "'%s'\n", err_replies[ERR_CONNECT]);
                return ERR_CONNECT;
        }

        /* check if indeed is our server */
        int nr = 0;
        memset(buf, 0, 1024);

        nr = read(sfd, &buf, 1024);
        if (nr > 0) {
                if (strncasecmp(buf, banner, xstrlen(banner)) == 0) {
                        fprintf(stderr, "[+] Got banner from '%s'\n", ip);
                } else {
                        fprintf(stderr, "[-] Invalid banner. Wrong server maybe ?\n");
                        exit(ERR_PROTO);
                }
                        
        } else {
                exit(ERR_PROTO);
        }

        if (is_valid_srv(sfd) == -1) {
                fprintf(stderr, "[+] server sent PONG\n");
                exit(ERR_PROTO);
        }

        switch (get_command(comm)) {
                case CMD_RETRIEVE:
                        if (!retrieve_flag(sfd, flagid, flag)) {
                                fprintf(stderr, "[-] flag '%s' not present on server '%s'\n", flagid, ip);
                                return ERR_WRONGFLAG;
                        } else {
                                fprintf(stderr, "[+] flag '%s' found on server '%s'\n", flagid, ip);
                                return OK;
                        }
                break;
                case CMD_STORE:
                        if (!store_flag(sfd, flagid, flag)) {
                                fprintf(stderr, "[-] could not store '%s' ('%s') on server '%s'\n", flagid, flag, ip);
                                return ERR_GENERIC;
                        } else {
                                fprintf(stderr, "[+] server '%s' sent OK\n", ip);
                                return OK;
                        }
                break;
		case CMD_LKEYS:
			ssend(sfd, CMD_LKEYS);
			readData(sfd);
		break;
		case CMD_CKEYS:
			ssend(sfd, CMD_CKEYS);
			readData(sfd);
		break;
                default:
                        return ERR_GENERIC;
                break;
        }

        close(sfd);

        xfree(comm);
        xfree(ip);
        xfree(port);
        xfree(flagid);
        xfree(flag);
        return 0;
}
