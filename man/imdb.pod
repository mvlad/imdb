=head1 NAME

IMDB

=head1 SYNOPSIS

B<imdb> [OPTIONS]

=head1 DESCRIPTION

Simple in memory database. IMDB uses a hash table to store I<key>, I<value>
pairs. 

=head1 OPTIONS 

=over

=item -d 

do not fork

=item -v

run with verbose mode, implies -d

=item -p <port>

specify other port to bind to

=item -h 

help page

=item -V

display version

=back

=head1 PROTOCOL SPECIFICATION

=over

=item REQUEST

depending on the type of I<COMMAND> optional attributes
might be required.

C<<
!LENGTH_OF_REQUEST!COMMAND![<OPTIONAL_ATTR1>]![<OPTIONAL_ATTR2>]\r\n
>>

=item RESPONSE

a response might be either a value (I<VALUE>) an acknowledge
(I<+PONG> or I<+OK>) or a error (I<-1>).

C<<
[<-1|+PONG|+OK|VALUE>]\r\n
>>

=item COMMANDS

Some basic commands are available:

=over

=item B<ping>

send a PING request to the server.

=item B<store> <mykey> <myvalue>

store I<myvalue> under I<mykey>

=item B<get> <key>

retrieve a value for key I<key>

=item B<incr> <key>

increment and retrieve contents of key I<key> (given that the
content of the key is an integer).

=item B<script>

invoking perl is done by using I<script> command:

B<NOTE>: make sure you end your script with a return other, 
otherwise you will not retrieve the contents of the I<evaluated>
perl script.

C<<
{ $sx = 100; $sp = 23; $sp *= $sx * $_ for (1 .. 100); $sp }
>>

=item B<list>

iterate and return all available keys in the database.

B<NOTE:> while this command is desirable when 
connections are coming from localhost, doing so for
all connections might I<not> be desirable.

=item B<count>

count all keys in the database.

=back

=back

=head1 RE-COMPILING

I<imdb> requires I<perl> development libraries to 
compile succesfully. Under a GNU/Linux machine this 
might be equivalent to I<libperl-dev> library.

=over

=item Normal way

You can re-compile IMDB by issuing a ``make''
in the root source directory. 

=item Compiling on a 64-bit machine a 32 bit version:


Download I<perlbrew>:

C<$ curl -kL http://install.perlbrew.pl | bash>

Init I<perlbrew>:

C<$ source ~/perl5/perlbrew/etc/SHELL>

where I<SHELL> is the shell you're currently running (i.e.: I<bash>, I<zsh>).

Install a 32-bit perl version: 

C<$ perlbrew install perl-5.14.3 -Accflags='-m32 -march=i686' -Aldflags='-m32 -march=i686' -Alddlflags='-shared -m32 -march=i686'>

Switch to it:

C<$ perlbrew use perl-5.14.3>

Then you can compile I<imdb>:

=item Compiling:

C<$ make>

B<NOTE:> in a new environment you'll need to switch to the 32 bit perl version, 
or you can add the C<$ source ~/perl5/perlbrew/etc/SHELL> to your SHELL 
F<rc configuration file>.

=back

=head1 BUGS

Probably a lot.

=head1 AUTHORS

This manual page has been written by Marius Vlad (L<mv@sec.uni-passau.de>).

=head1 SEE ALSO

L<http://redis.io>

=cut
