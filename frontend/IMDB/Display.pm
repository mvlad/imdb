package IMDB::Display;

use nginx;
use 5.014;

use IO::Socket;
use IO::Select;
use MIME::Base64;

use List::Util qw/shuffle/;
use Image::ExifTool qw/:Public/;

use constant DEBUGFILE	=> '/tmp/debug.nginx';
use constant ROOTDIRPNG => '/usr/share/nginx/www/unpacked';

sub DEBUG
{
	open my $fh, '>>', DEBUGFILE
		or die $!;
	print $fh $_[0];
	close $fh;
}

sub take
{
	my $vals = shift;
	my $args = shift;
	
	my $ret = [];

	my @rands = @$args;

	for my $i (1 .. $vals) {
		push @$ret, $rands[$i];
	}

	return $ret;
}

sub findFiles 
{
	my $req = shift;

	if (!-d ROOTDIRPNG) {
		return ();
        }

	opendir my $dirh, ROOTDIRPNG or
		$req->print("# Could not open " . ROOTDIRPNG . "<br />");

	my @files = grep { -f ROOTDIRPNG . '/' . $_ } readdir $dirh;
	closedir $dirh;

	return \@files;
}

sub create_sock
{
	my ($target, $port) = @_;
	my $s = IO::Socket::INET->new(PeerAddr => $target, 
				      PeerPort => $port, 
                                      Blocking => 0, 
                                      Proto => 'tcp')
	or return undef;
	my $banner = <$s>;

	if ($banner =~ /InMemory/) {
		return $s;
	}
	return undef;
}

sub action
{
	my ($action, $sock, $data) = @_;
	
	my $str = '';
	my $rr  = '';
	my $len = 0;

	given ($action) {
		when (/^get$/) {
			if (!defined $data) {
				return;
			}
			$str = "!%d!get!%s\r\n";
			$len = length($action) + length($data) + 4;
			$rr = sprintf $str, $len, $data;
		}
		when (/^count$/) {
			$str = "!%d!count\r\n";
			$len = length $action + 4;
			$rr = sprintf $str, $len;

		}
		when (/^list$/) {
			$str = "!%d!list\r\n";
			$len = length $action + 4;
			$rr = sprintf $str, $len;
		}
		default {
			# invalid ...
		}
	}

	# send data
	print $sock $rr;

	my $len = 0;
	my $tbuf = '';
	my $sum = 0;
	# block 'till we get something...
	my $io = new IO::Select($sock);

	while (my @ready = $io->can_read(0.1)) {

		for my $fd (@ready) {
			if ($fd == $sock) {
				$len = sysread($sock, my $tmpbuf, 0x100);
				$tbuf .= $tmpbuf;
				$sum  += $len;
			}
		}

	}
	return $tbuf;
}


sub getvalue
{
	my ($sock, $key) = @_;

	my $value = action "get", $sock, $key;
	return $value if $value;
	return undef;
}

sub getkeys
{
	my $sock = shift;
	my $req  = shift;
	my $keys = [];

	my $value = action "list", $sock;

	# get the count ammount
	# 'NUMBER\r\nflagid\r\n'
	my @items = split /\r\n/, $value;
	my $count = shift @items;

	if (scalar $count > 0) {
		@$keys = @items;
	}

	return $keys;
}

sub hreq
{
	my $req = shift;
	my $sock = shift;

	
	# is it really you ?
	given ($req->uri) {

		when (/k=(\w+)/) {
			my $key = action "get", $sock, $1;
			if ($key) {
				$req->print("Key $1 has $key <br />");
			}
		}

		default {
			my $c = action "count", $sock;
			my $nr = scalar $c;
			$req->print("Total items in db: $nr <br />");
			if ($nr > 0) {
				my $raw_items = action "list", $sock;
				my @keys = split /\r\n/, $raw_items;
				for my $key (@keys) {
					$req->print("Key: $key <br />");
				}
			}
		}

	}

	close $sock;
}

sub readImage
{
	my $fn = shift;

	if (! -f $fn) {
		return;
	}

	my $file = '';
	my $len;
	my $sum;

	$sum = $len = 0;

	open my $fh, '<', $fn;
	while ($len = sysread $fh, my $tbuf, 0xff) {
		$sum += $len;
		$file .= $tbuf;
	}
	close $fh;

	return encode_base64($file);
}

sub displayImage
{
	my ($req, $data) = @_;
	my $html = "<img alt=\"Embedded Image\" src=\"data:image/png;base64,$data\" />";
	$req->print($html);
}

sub writeKeyWord
{
	my $fn = shift;
	my $data = shift;

	my %opts = ();

	my $exif = new Image::ExifTool;
	$exif->ExtractInfo($fn, \%opts);

	my $p = $exif->SetNewValue(Keywords => "$data");
	# commit
	$exif->WriteInfo($fn);
	return $p;
}

sub writeKeyWords
{
	my $fn = shift;
	my $values = shift;

	my %opts = ();

	my $exif = new Image::ExifTool;
	$exif->ExtractInfo($fn, \%opts);

	my $p = $exif->SetNewValue(Keywords => $values);
	# commit
	$exif->WriteInfo($fn);
	return $p;
}

# home-page
sub handler 
{
	my $req = shift;


	$req->send_http_header("text/html");
	return OK if $req->header_only;

	$req->print("<h1> Oh Hai, Can I Has Prtty ICOn?</h1> <br />");


	my $pngs = findFiles $req;
	if (scalar $pngs) {

		# init connection to db
		my $sock = create_sock('localhost', 3769, $req);
		if (!defined $sock) {
			$req->print("Could not establish connection <br />");
			return OK;
		}

		my $keys = getkeys $sock, $req;
		my $tkeys = scalar @$keys;

		if ($tkeys) {
			
			$req->print("<input type=\"hidden\" value=\"$tkeys\" />");

			my @krands = shuffle @$keys;
			# my $imgs   = take $tkeys, $pngs;

			my @prands  = shuffle @$pngs;
			my $img     = shift @prands;

			writeKeyWords(ROOTDIRPNG . '/' . $img, \@krands);
			my $b64 = readImage ROOTDIRPNG . '/' . $img;
			displayImage($req, $b64);

		} else {
			$req->print("# No available flags... :-( <br />");
		}
	} else {
		$req->print("No files found under " . ROOTDIRPNG . "<br />");
	}

	return OK;
}

1;
__END__
