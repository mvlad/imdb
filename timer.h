#ifndef __TIMER_H
#define __TIMER_H

struct timer;
typedef int (*timer_cb)(struct timer *, void *);

struct timer {
        struct list_head list;  /* link to the timers */
        struct timeval tv;      /* when should I execute this timer */
        struct timeval set_at;  /* when did we set it ... */
        timer_cb cb;            /* function to be executed */
        void *data;             /* additional data for cb */
        unsigned int timeout;   /* original timeout, in ms */
};

extern struct timer *create_timer(timer_cb cb, void *data);
extern void insert_timer(struct timer *tm);
extern void cancel_timer(struct timer *tm);
extern void set_timer(struct timer *tm, unsigned int ms);
extern void destroy_timer(struct timer *tm);
extern int status_timer_cb(struct timer *tm, void *data);
extern int thread_timer_cb(struct timer *tm, void *data);
extern inline unsigned int compute_timeout(void);
extern void do_timers(void);


#endif
