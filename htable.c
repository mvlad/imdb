#include "common.h"
#include "htable.h"
#include "xutil.h"

hash_internal_t
**hash_create(unsigned int n_buckets, unsigned int thread_cnt,
              k_hash_func hash_func, k_compare_func compare_func,
              k_val_destroy_func destroy_func)
{
        hash_internal_t *hash; 
        hash_internal_t **ret;
        unsigned int i;

        if (thread_cnt < 0) {
                return NULL;
        }

        if (n_buckets < 256)
                n_buckets = 256;

        hash = xcalloc(1, sizeof(hash_internal_t));

        hash->n_buckets         = n_buckets;
        hash->hash_func         = hash_func;
        hash->compare_func      = compare_func;
        hash->destroy_func      = destroy_func;

        hash->buckets = xmalloc(n_buckets * thread_cnt * sizeof(struct list_head));

        for (i = 0; i < hash->n_buckets; i++) {
                INIT_LIST_HEAD(&hash->buckets[i]);
        }

        ret = xmalloc(sizeof(hash_internal_t *));
        *ret = hash;

        return ret;
}

static hash_entry_t
*hash_lookup_internal(hash_internal_t **ihash, void *key)
{
        hash_internal_t *hash = *ihash;
        hash_entry_t    *he;
        unsigned int bucket;

        bucket = hash->hash_func(key) % hash->n_buckets;
        DEBUG("# (add/found) bucket: '%d'\n", bucket);

        list_for_each_entry(he, &hash->buckets[bucket], list) {
                DEBUG("# (comparing %s with %s)\n", (char *) he->key, (char *) key);
                if (hash->compare_func(he->key, key) == 0) {
                        DEBUG(">>> FOUND <<<\n");
                        return he;
                }
        }
        return NULL;
}

void *hash_lookup(hash_internal_t **ihash, void *key)
{
        hash_entry_t *he = hash_lookup_internal(ihash, key);

        if (he) {
                return he->val;
        }
        return NULL;
}

void hash_remove(hash_internal_t **ihash, void *key)
{
        hash_entry_t    *he = hash_lookup_internal(ihash, key);
        hash_internal_t *hash = *ihash;

        if (!he) {
                return;
        }

        list_del(&he->list);

        hash->destroy_func(he->key, he->val);

        xfree(he);
        hash->n_entries--;
}

static void
hash_grow(hash_internal_t **ihash)
{
}

void hash_insert(hash_internal_t **ihash, void *key, void *val)
{
        hash_internal_t *hash = *ihash;
        hash_entry_t    *he;
        unsigned int hv, bucket;

        hv = hash->hash_func(key);
        bucket = hv % hash->n_buckets;

        list_for_each_entry(he, &hash->buckets[bucket], list) {
                if (hash->compare_func(he->key, key) == 0)
                        goto got_item;
        }
        if ((hash->n_entries * HASH_GROW_THRESHOLD) / 100 >= hash->n_buckets) {
                hash_grow(ihash);

                hash = *ihash;
                bucket = hv % hash->n_buckets;
        }

        he = xmalloc(sizeof(hash_entry_t));
        he->key = key;
        he->val = val;

        list_add_tail(&he->list, &hash->buckets[bucket]);
        hash->n_entries++;

        return;

got_item:
        hash->destroy_func(he->key, he->val);

        he->key = key;
        he->val = val;
}
